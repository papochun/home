#! /bin/sh

export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_CONFIG_HOME="${HOME}/.config";
export XDG_DATA_HOME="${HOME}/.local/share";
export XDG_STATE_HOME="${HOME}/.local/state";

append_data() {
  XDG_DATA_DIRS="${XDG_DATA_DIRS}:${1}" 
}

append_data "/var/lib/flatpak/exports/share"
append_data "${HOME}/.local/share/flatpak/exports/share"
append_data "${HOME}.local/share"

# wayland
export WLR_NO_HARDWARE_CURSORS="1"

# compiler
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export RUSTUP_HOME="${XDG_DATA_HOME}/rustup"
export GOPATH="${XDG_DATA_HOME}/go"

# interaction
export OPENER="xdg-open"
export BROWSER="xdg-open"
export TERMINAL="kitty"
export EDITOR="helix"
export VISUAL="${EDITOR}"
export BASH_ENV=".bashrc"

# misc
export WINEPREFIX="${XDG_DATA_HOME}/wineprefixes/default"
export CUDA_CACHE_PATH="${XDG_CACHE_HOME}/nv"
export LESSHISTFILE="/dev/null"
export HISTFILE="/dev/null"
export ABDUCO_SOCKET_DIR="${XDG_CONFIG_HOME}"
export GTK_THEME='adw-gtk3-dark'

# used for input remapper idk
export NO_AT_BRIDGE="1"

append_path() {
  export PATH="${PATH}:${1}" 
}

append_path "${HOME}/.local/bin"
append_path "${XDG_DATA_HOME}/cargo/bin"
append_path "."
