require("plugins")
require('lualine').setup()
require('bufferline').setup()
require("scrollbar").setup()
require('gitsigns').setup()
require('smartcolumn').setup({
  colorcolumn = "70",
})
require('leap').add_default_mappings()
require('neoscroll').setup({
    easing_function = "sine"
})
vim.cmd.colorscheme("wallbox")

local t = {}
t['<C-u>'] = {'scroll', {'-vim.wo.scroll', 'true', '250'}}
t['<C-d>'] = {'scroll', { 'vim.wo.scroll', 'true', '250'}}
t['<C-b>'] = {'scroll', {'-vim.api.nvim_win_get_height(0)', 'true', '500'}}
t['<C-f>'] = {'scroll', { 'vim.api.nvim_win_get_height(0)', 'true', '500'}}
t['<C-y>'] = {'scroll', {'-0.10', 'false', '100'}}
t['<C-e>'] = {'scroll', { '0.10', 'false', '100'}}
t['zt']    = {'zt', {'300'}}
t['zz']    = {'zz', {'300'}}
t['zb']    = {'zb', {'300'}}

require('neoscroll.config').set_mappings(t)


vim.cmd("set ignorecase")
vim.cmd("set autoindent")
vim.cmd("set relativenumber")
vim.cmd("set cursorline")
vim.cmd("set clipboard+=unnamedplus")

vim.cmd("set expandtab")
vim.cmd("set tabstop=2")
vim.cmd("set softtabstop=0")
vim.cmd("set shiftwidth=0")

vim.keymap.set('n', 'gh', '0')
vim.keymap.set('n', 'gl', '$')

vim.g.mapleader = ' '
vim.keymap.set('n', '<Leader>h', '<cmd>bprevious<CR>')
vim.keymap.set('n', '<Leader>l', '<cmd>bnext<CR>')
vim.keymap.set('n', '<Leader>q', '<cmd>bdelete<CR>')
vim.keymap.set('n', '<Leader>n', '<cmd>NnnPicker<CR>')
vim.keymap.set('n', '<Leader>pi', '<cmd>PackerInstall<CR>')
vim.keymap.set('n', '<Leader>pu', '<cmd>PackerUpdate<CR>')
vim.keymap.set('n', '<Leader>ps', '<cmd>PackerSync<CR>')
