require('vis')

vis:map(vis.modes.NORMAL, ' y', ':> wl-copy 2>/dev/null -n<Enter>')
vis:map(vis.modes.VISUAL, ' y', ':> wl-copy 2>/dev/null -n<Enter>')
vis:map(vis.modes.VISUAL_LINE, ' y', ':> wl-copy 2>/dev/null -n<Enter>')
vis:command("set tab 2")
vis:command("set expandtab on")
