#! /bin/sh
set -o vi

export GPG_TTY=$(tty)

git_prompt () {
	branch="$(git rev-parse --abbrev-ref HEAD 2>/dev/null)"
	test "$branch" || return
	printf -- "%s" "[$branch] "
}
export PS1="\$(git_prompt)\$ "

export NNN_OPENER="xdg-open"
export NNN_TRASH="1"
n ()
{
    [ "${NNNLVL:-0}" -eq 0 ] || {
        echo "nnn is already running"
        return
    }

    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    command nnn -RTv "$@"

    [ ! -f "$NNN_TMPFILE" ] || {
        . "$NNN_TMPFILE"
        rm -f "$NNN_TMPFILE" > /dev/null
    }
}

a () {
	tmux new -A -s "$1" "$@"
}
